import { ADD_TO_CART, REMOVE_FROM_CART } from '../constants'
import { bake_cookie, read_cookie, delete_cookie} from 'sfcookies'

const item = (action) => {
  return {
    product: action.product,
    quantity: action.quantity,
    price: action.price * action.quantity,
    id: Math.random()
  }
}

const removeById = (state=[], id) => {
  const items = state.filter(item => item.id != id)
  return items
}

const items = (state = [], action) => {
  let items = null;
  state = read_cookie('items')
  switch(action.type) {
    case ADD_TO_CART:
      items = [...state, item(action)]
      console.log('item as state', items)
      bake_cookie('items', items)
      return items
    case REMOVE_FROM_CART:
      items = removeById(state, action.id)
      console.log('REMOVE_FROM_CART', items)
      bake_cookie('items', items)
      return items
    default:
      return state;
  }
}

export default items
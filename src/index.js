import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {Provider} from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers'
import FontAwesome from 'react-fontawesome'
import * as firebase from 'firebase'
import './index.css';
import App from './components/App';
import Product from './components/Product';
import registerServiceWorker from './registerServiceWorker';
import Cart from './components/cart';
import Products from './components/Products';
import Press from './components/Press';
import Favicon from 'react-favicon';

 const store = createStore(reducer);

var config = {
    apiKey: "AIzaSyDahHbkprQDBZ7oWuF36Pdbr5n52SzJhJY",
    authDomain: "dobandar-2392f.firebaseapp.com",
    databaseURL: "https://dobandar-2392f.firebaseio.com",
    projectId: "dobandar-2392f",
    storageBucket: "",
    messagingSenderId: "25539137403"
};

firebase.initializeApp(config);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
      <Favicon url='./images/favicon.ico'/>
        <header className="full_wrapper">
          <div className="logo">
            <a href="/">
              <img src={require('./images/db_logo.png')} alt='Logo'/>
            </a>
          </div>
          <nav className="main-nav">
              <div className="menu-content menu-container">
                <div className="menu-content-wrap menu">
                  <ul id="nav" className="menu-nav">
                    <li className="pull-left"><a href="/">Store Locator</a>
                    </li>
                    <li className="pull-left"><a href="/">Shop</a>
                      <ul>
                        <li><a href="#">Face</a></li>
                        <li><a href="#">Feet</a></li>
                        <li><a href="#">Body</a></li>
                      </ul>
                    </li>
                    <li className="pull-left" style={{paddingLeft: '900px'}}><a href='/cart'><FontAwesome 
                          name='shopping-cart'
                          size='2x' 
                        /> </a>
                    </li>
                  </ul>
                </div>
              </div>
          </nav>
        </header>
        <Route exact path="/" component={App} />
        <Route path="/product/:name" component={Product} />
        <Route path="/cart" component={Cart} />
        <Route path="/products" component={Products} />
        <Route path="/press" component={Press} />
        <footer>
            <div className="before_footer">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4">
                            <h3 className="before_footer_title">About us</h3>
                            <p>Quisque congue accumsan antem id condimentum aliquam lorem.</p>
                            <div>
                                <address> <strong> 10 Old Street Malleshwaram, <br /> # 404 SevaSadan , Bengaluru 560056 </strong></address>
                                <p><strong>+91 0321 28156058</strong>
                                    <br />
                                    <strong>info@dobandar.com</strong></p>
                            </div>
                        </div>
                        <div className="col-sm-2">
                            <div className="widget_nav_menu">
                                <h3 className="before_footer_title">Product</h3>
                                <ul className="">
                                    <li><a href="/press">Press</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Integrate &amp; Enterprise</a></li>
                                    <li><a href="#">Affileate Program</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-offset-2 col-sm-4">
                            <h3 className="before_footer_title">News update</h3>
                            <div className="news_feed">
                                <input type="text" name="name" id="name" required="" placeholder="Enter Name" />
                                <input type="submit" value="Submit" className="send_button" name="email-send" />
                            </div>
                            <p>Quisque congue accumsan ante id condimentum. Aliquam lorem nisl, bibendum et sollicitudin ut, viverra vitae ligula.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="footer_container">
                <div className="container">
                    <div className="copyright">
                        <div className="row">
                            <div className="col-sm-6">
                                <p>
                                    Copyright 2017 © DOBANDAR
                                </p>
                            </div>
                            <div className="col-sm-6">
                                <ul className="footer_nav text-right">
                                    <li>
                                        <a href="#" target="blank" title="Facebook">Facebook</a>
                                    </li>
                                    <li>
                                        <a href="#" target="blank" title=" Twitter">Twitter</a></li>
                                    <li><a href="#" target="blank " title="Dribbble ">Dribbble</a></li>
                                    <li><a href="#" target="blank " className="color youtube " title="YouTube ">YouTube</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
      </div>
    </Router>
  </Provider>, document.getElementById('root'))
registerServiceWorker();

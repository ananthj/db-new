import React, { Component } from 'react';
// import * as firebase from 'firebase'
import '../App.css';

class Products extends Component {

  constructor(props){
    super(props)
  }

  render() {
    return(
      <div>
            <div class="container mb_80">
                <div class="row mt_40">
                    <div class="col-sm-12">
                        <ul class="work_filter">
                            <li class="active"><span data-filter="*" class="active">All</span></li>
                            <li><span data-filter=".branding">Branding</span></li>
                            <li><span data-filter=".illustration">Illustration</span></li>
                            <li><span data-filter=".photography">Photography</span></li>
                            <li><span data-filter=".web-design">Web design</span></li>
                        </ul>
                        <div class="et_three_col et_padding_15 masonry_layout">
                            <div class="work_item photography illustration">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/Products/Activated_charcoal.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="/product/Activated Charcoal Soap">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="/product/Activated Charcoal Soap">Activated Charcoal Soap</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item web-design photography">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/Products/db1.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">From music to art</a></h2>
                                    <h5>Development & Branding </h5>
                                </div>
                            </div>
                            <div class="work_item web-design illustration">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Project Campaign</a></h2>
                                    <h5>Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item web-design photography">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Brand Front store</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item branding illustration">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Fitness club branding</a></h2>
                                    <h5>Development & Branding</h5>
                                </div>
                            </div>
                            <div class="work_item branding photography illustration">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Brand design & App</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item photography branding">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                        <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Product Design</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item web-design branding">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Fitness club branding</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item web-design photography">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">New brand design</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item web-design photography">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">From music to art</a></h2>
                                    <h5>Development & Branding </h5>
                                </div>
                            </div>
                            <div class="work_item web-design illustration">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">Project Campaign</a></h2>
                                    <h5>Branding Campaign</h5>
                                </div>
                            </div>
                            <div class="work_item web-design photography">
                                <div class="work_img">
                                    <div class="hover_effect">
                                        <figure class="effect-bubba">
                                            <img src={require('../images/12.jpg')} alt="Wedding Picture" />
                                            <figcaption>
                                                <div class="view_more_icon"><i class="ion-arrow-right-c" aria-hidden="true"></i></div>
                                                <a href="portfolio-single-1.html">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="work_item_title">
                                    <h2><a href="portfolio-single-1.html">New brand design</a></h2>
                                    <h5>Digital Branding Campaign</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
      </div>
    )
  }
}

export default Products;
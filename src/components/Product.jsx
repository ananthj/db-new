import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../actions';
import * as firebase from 'firebase'
import '../App.css';

class Product extends Component {
  constructor(props){
    super(props);
    this.state = {
      productName: props.match.params.name,
      quantity: 1,
      price: 0,
      productParams: [],
      productAvailable: false
    }
  }

  addItems(){
    this.props.addToCart(this.state.productName, this.state.quantity, this.state.price)
  }

  increaseQuantity(){
    const quantity = this.state.quantity + 1
    this.setState({quantity})
  }

  decreaseQuantity(){
    const quantity = this.state.quantity - 1
    if (quantity > 0) {
      this.setState({quantity})
    }
  }

  componentWillMount(){
    const test = firebase.database().ref().child('products/'+this.state.productName)
    test.on('value', snap => {
      this.setState({productParams: snap.val(), productAvailable: true, price: snap.val().MRP})
    })
  }

  render() {
    const productParams = this.state.productParams
    return(
      <div>
      {
        this.state.productAvailable ? 
        <div id="main-content" className="m-scene">
        <div className="home_1_testimonials_cont">
          <div className="container">
              <div className="row">
                  <div className="testimonials_title">
                      <h2>{this.state.productName}</h2>
                  </div>
              </div>
          </div>
          </div>
                        
          <div className="page-container scene-main scene-main--fadein">
              <div className="page_loading">
                  <div className="spinner"></div>
              </div>
              <div className="container">
                  <div className="row">
                      <div className="col-lg-12">
                          <div className="single-product-content">
                              <div className="single-product-images popup_gallery"><img src={productParams.image_url} alt="Product Image" />
                                  <div className="clearfix"></div>
                              </div>
                              <div className="single-product-des">
                                  <h2 className="product-title">{this.state.productName}</h2>
                                  <div className="add-to-cart-warp">
                                          <div className="quantity">
                                              <input type="button" value="-" className="minus" onClick={() => this.decreaseQuantity()}/>
                                              <input type="text" name="quantity" value={this.state.quantity} title="Qty" className="total-quantity" />
                                              <input type="button" value="+" className="plus" onClick={() => this.increaseQuantity()}/>
                                          </div>
                                          <a href="#" className="button add-cart-btn" onClick={() => this.addItems()}>Add To Cart </a>
                                          <div className="clearfix"></div>
                                      </div>
                                  <div className="single-product-rating">
                                      <a href="#">(1 customer review)</a>
                                  </div>
                                  <div className="single-product-price">Rs. {productParams.MRP}/-</div>
                                  <div className="single-product-deta">
                                      <p><b>Product Description:</b> {productParams.product_description}</p>

                                      <p><b>Variant:</b> {productParams.varient}</p>

                                      <p><b>Category:</b> {productParams.Category}</p>

                                      <p><b>Material Used:</b> {productParams.Ingredients}</p>

                                      <p><b>Indications:</b> {productParams.Indications}</p>

                                  </div>
                              </div>
                          </div>
                          <div className="clearfix"></div>
                          <div className="related-products">
                              <div className="special_title under_line_title mb_60">
                                  <h3>RELATED PRODUCTS</h3>
                              </div>
                              <ul className="products et_four_col">
                                  <li>
                                      <a href="#" className="product_link">
                                          <img src={require("../images/Products/db2.jpg")} />
                                          <h3>Fresh Soap</h3>
                                          <span className="price">Rs. 99.00</span>
                                          <span className="rating"> 
                                          <span className="star"></span>
                                          <span className="star"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          </span>
                                      </a>
                                      <a href="#" className="add_to_cart_button button">Buy</a>
                                  </li>
                                  <li>
                                      <a href="#" className="product_link">
                                          <img src={require("../images/Products/db3.jpg")} />
                                          <h3>Bath Oil</h3>
                                          <span className="price">Rs. 599.00</span>
                                          <span className="rating"> 
                                          <span className="star"></span>
                                          <span className="star"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          </span>
                                      </a>
                                      <a href="#" className="add_to_cart_button button">Buy</a>
                                  </li>
                                  <li>
                                      <a href="#" className="product_link">
                                          <img src={require("../images/Products/db4.jpg")} />
                                          <h3>Citrus Sugar Scrub</h3>
                                          <span className="price">Rs. 499.00</span>
                                          <span className="rating"> 
                                          <span className="star"></span>
                                          <span className="star"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          </span>
                                      </a>
                                      <a href="#" className="add_to_cart_button button">Buy</a>
                                  </li>
                                  <li>
                                      <a href="#" className="product_link">
                                          <img src={require("../images/Products/db5.jpg")} />
                                          <h3>Lip Balm</h3>
                                          <span className="price">Rs. 99.00</span>
                                          <span className="rating"> 
                                          <span className="star"></span>
                                          <span className="star"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          <span className="star star-rated"></span>
                                          </span>
                                      </a>
                                      <a href="#" className="add_to_cart_button button">Buy</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div> : <div>  </div>
      }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    items: state
  }
}

export default connect(mapStateToProps, {addToCart})(Product); 
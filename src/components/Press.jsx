import React, { Component } from 'react';
import { connect } from 'react-redux';

class Press extends Component {
	constructor(props){
		super(props);
	}
	render() {
		return(
			<div id="main-content" className="m-scene">
				<div className="home_1_testimonials_cont">
          			<div className="container">
              			<div className="row">
                  			<div className="testimonials_title">
                      			<h2>Press</h2>
                  			</div>
              			</div>
          			</div>
          		</div>


          		<div className="home_1_service_cont">
                <div className="mt_20 page-container scene-main scene-main--fadein">
                    <div className="row">
                        <div className="col-sm-12">
                            <ul className="et_gallery et_four_col popup_gallery masonry_layout et_padding_10">
                                <li>
                                   	<img src={require('../images/press/01.jpg')} alt="Product Image" /> 
                                </li>
                                <li>
                                    <img src={require('../images/press/03.jpg')} alt="Product Image" />
                                </li>
                                <li>
                                    <img src={require('../images/press/06.jpg')} alt="Product Image" />
                                </li>
                                <li>
                                    <img src={require('../images/press/04.jpg')} alt="Product Image" />
                                </li>
                                <li>
                                    <img src={require('../images/press/05.jpg')} alt="Product Image" />
                                </li>
                                <li>
                                    <img src={require('../images/press/02.jpg')} alt="Product Image" />
                                </li>
                                <li>
                                    <img src={require('../images/press/08.jpg')} alt="Product Image" />
                                </li>
                                <li>
                                    <img src={require('../images/press/09.jpg')} alt="Product Image" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
			</div>
			)
	}
}

							


export default Press; 
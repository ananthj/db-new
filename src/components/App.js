import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../actions';
import Script from 'react-load-script'
import OwlCarousel from 'react-owl-carousel';
import db_logo from '../images/db_logo.png';


import '../App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  addToCart(product, quantity, price){
    this.props.addToCart(product, quantity, price)
  }

  render() {
    return (
      <div> 
        <div className="scheme_color_norway">
          <div id="main-content" className="m-scene">
            <div className="page-container scene-main scene-main--fadein">
              <div className="page_loading">
                  <div className="spinner"></div>
              </div>
              <div id="second_carousel" className="owl-carousel owl-theme thirdhalf_height simple_slider">
                  <div className="item">
                      <img className="img_as_bg" src={require("../images/home/30.jpg")} alt="The Last of us" />
                      <div className="slide_content light_color ">
                          <h1 className="big_title title_bold mb_15">DO BANDAR</h1>
                          <p className="big_description">Luxurious Natural Body Care <b>100% Free</b> from synthetics, synthetic fragrances.</p>
                      </div>
                  </div>
                  <div className="item">
                      <img className="img_as_bg" src={require("../images/home/31.jpg")} alt="The Last of us" />
                      <div className="slide_content light_color ">
                        <h1 className="big_title title_bold mb_15">DO BANDAR</h1>
                        <p className="big_description">Luxurious Natural Body Care <b>100% Free</b> from synthetics, synthetic fragrances.</p>
                      </div>
                  </div>
                  <div className="item ">
                      <img className="img_as_bg" src={require("../images/home/32.jpg")} alt="Mirror Edge" />
                      <div className="slide_content light_color ">
                        <h1 className="big_title title_bold mb_15">DO BANDAR</h1>
                        <p className="big_description">Luxurious Natural Body Care <b>100% Free</b> from synthetics, synthetic fragrances.</p>
                      </div>
                  </div>
              </div>
              <div className="container">
                <div className="row">
                    <div className="col-sm-7">
                        <div className="special_title side_line_title mt_100 mb_80">
                            <h2>New Favourites</h2>
                            <p>Quisque congue accumsan ante id condimentum. Aliquam lorem nisl, bibendum et sollicitudin ut, viverra vitae ligula. In hac habitasse platea dictumst. Quisque in tincidunt augue. Mauris tempus pulvinar feugiat.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container mb_60">
                <div className="row">
                    <ul className="products">
                        <li data-aos="">
                            <a href="/product/Activated Charcoal Soap" className="product_link">
                                <img src={require("../images/Products/Activated_charcoal.jpg")} />
                                <h3>Activated Charcoal Soap</h3>
                                <span className="price">&#x20B9;200.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Activated Charcoal Soap', 1, 200)} className="add_to_cart_button button">Buy</a>
                        </li>
                        <li data-aos="">
                            <a href="/product/Basil Camphor Turmeric Soap" className="product_link">
                                <img src={require("../images/Products/Basil-Camphor-Turmeric-Soap.jpg")} />
                                <h3>Basil Camphor Turmeric Soap</h3>
                                <span className="price">&#x20B9;170.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Basil Camphor Turmeric Soap', 1, 300)} className="add_to_cart_button button">Buy</a>
                        </li>
                        <li data-aos="">
                            <a href="/product/Cocoa Butter Soap" className="product_link">
                                <img src={require("../images/Products/Cocoa_Butter_Soap.jpg")} />
                                <h3>Cocoa Butter Soap</h3>
                                <span className="price">&#x20B9;300.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Cocoa Butter Soap', 1, 300)} className="add_to_cart_button button">Buy</a>
                          </li>
                        </ul>
                      </div>
                      <div className="row">
                          <div className="col-sm-7">
                              <div className="special_title side_line_title mt_100 mb_80">
                                  <h2>Pets</h2>
                                  <p>Quisque congue accumsan ante id condimentum. Aliquam lorem nisl, bibendum et sollicitudin ut, viverra vitae ligula. In hac habitasse platea dictumst. Quisque in tincidunt augue. Mauris tempus pulvinar feugiat.</p>
                              </div>
                          </div>
                      </div>
                      <div className="row">
                          <ul className="products">
                        <li data-aos="">
                            <a href="/product/Lavandin & Ylang Ylang Soap" className="product_link">
                                <img src={require("../images/Products/Lavandin_ylangylangSoap.jpg")} />
                                <h3>Lavandin & Ylang Ylang Soap</h3>
                                <span className="price">&#x20B9;300.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Lavandin & Ylang Ylang Soap', 1, 300)} className="add_to_cart_button button">Buy</a>
                        </li>
                        <li data-aos="">
                            <a href="/product/Lemongrass Soap" className="product_link">
                                <img src={require("../images/Products/DoBandar_LemonGrassSoap_01.jpg")} />
                                <h3>Lemongrass Soap</h3>
                                <span className="price">&#x20B9;200.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Lemongrass Soap', 1, 200)} className="add_to_cart_button button">Buy</a>
                        </li>
                        <li data-aos="">
                            <a href="/product/Orange & Gram Flour Soap" className="product_link">
                                <img src={require("../images/Products/Do-Bandar_Orange-Gram-Flour-Soap_08.jpg")} />
                                <h3>Orange & Gram Flour Soap</h3>
                                <span className="price">&#x20B9;170.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Orange & Gram Flour Soap', 1, 170)} className="add_to_cart_button button">Buy</a>
                        </li>
                      </ul>
                    </div>
                    <div className="row">
                        <div className="col-sm-7">
                            <div className="special_title side_line_title mt_100 mb_80">
                                <h2>Home</h2>
                                <p>Quisque congue accumsan ante id condimentum. Aliquam lorem nisl, bibendum et sollicitudin ut, viverra vitae ligula. In hac habitasse platea dictumst. Quisque in tincidunt augue. Mauris tempus pulvinar feugiat.</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <ul className="products">
                        <li data-aos="">
                            <a href="/product/Patchouli Soap" className="product_link">
                                <img src={require("../images/Products/Do-Bandar_Patchouli-Soap-18.jpg")} />
                                <h3>Patchouli Soap</h3>
                                <span className="price">&#x20B9;170.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Patchouli Soap', 1, 170)} className="add_to_cart_button button">Buy</a>
                        </li>
                        <li data-aos="">
                            <a href="/product/Vanilla Cinnamon Soap" className="product_link">
                                <img src={require("../images/Products/Do-Bandar_Vanilla-Cinnamon-Soap_05.jpg")} />
                                <h3>Vanilla Cinnamon Soap</h3>
                                <span className="price">&#x20B9;170.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Vanilla Cinnamon Soap', 1, 170)} className="add_to_cart_button button">Buy</a>
                        </li>
                        <li data-aos="">
                            <a href="/product/Vetiver Soap" className="product_link">
                                <img src={require("../images/Products/Do-Bandar_Vetiver-Soap_17.jpg")} />
                                <h3>Vetiver Soap </h3>
                                <span className="price">&#x20B9;300.00</span>
                                <span className="rating">
                                <span className="star"></span>
                                <span className="star"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                <span className="star star-rated"></span>
                                </span>
                            </a>
                            <a href="/cart" onClick={() => this.addToCart('Vetiver Soap', 1, 300)} className="add_to_cart_button button">Buy</a>
                        </li>
                    </ul>
                    <div className="align-center mt_10" >
                        <a href="#" className="button button--itzel button--text-thick"><i className="button__icon ion-arrow-down-a"></i><span>LOAD MORE PRODUCT</span></a>
                    </div>
                </div>
            </div>
            </div>
            <div className="box_layout_wrapper">
              <div className="home_9_service_cont">
                  <div className="row">
                      <div className="col-sm-4">
                          <div className="service_box service_box_align_left" data-aos="fade-up">
                              <div className="service_icon"><i className="ion-ios-bolt" aria-hidden="true"></i></div>
                              <div className="service_info">
                                  <h3>Natural</h3>
                                  <p>Phasellus varius cursus turpis dign issim mollis. Vestibulum hendrerit arcu nunc, sit amet dignissim dui mollis maximus. Mauris tempus pulvinar feugiat.</p><a href="#" className="read_more">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div className="col-sm-4">
                          <div className="service_box service_box_align_left" data-aos="fade-up" data-aos-delay="200">
                              <div className="service_icon"><i className="ion-ios-loop-strong" aria-hidden="true"></i></div>
                              <div className="service_info">
                                  <h3>Healthy</h3>
                                  <p>Phasellus varius cursus turpis dign issim mollis. Vestibulum hendrerit arcu nunc, sit amet dignissim dui mollis maximus. Mauris tempus pulvinar feugiat.</p><a href="#" className="read_more">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div className="col-sm-4">
                          <div className="service_box service_box_align_left" data-aos="fade-up" data-aos-delay="400">
                              <div className="service_icon"><i className="ion-android-bicycle" aria-hidden="true"></i></div>
                              <div className="service_info">
                                  <h3>Genuine</h3>
                                  <p>Phasellus varius cursus turpis dign issim mollis. Vestibulum hendrerit arcu nunc, sit amet dignissim dui mollis maximus. Mauris tempus pulvinar feugiat.</p><a href="#" className="read_more">Read More</a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        <div className="home9_clients_carousel_cont">
            <div className="container">
                <div className="row">
                    <ul id="loop_carousel" className="owl-carousel client-logo-list">
                        <li>
                            <a href="/"> <img src={require("../images/press/BM.png")} alt="Energetic Themes" /></a>
                        </li>
                        <li>
                            <a href="/"> <img src={require("../images/press/LM.png")} alt="Energetic Themes" /></a>
                        </li>
                        <li>
                            <a href="/"> <img src={require("../images/press/toi.png")} alt="Energetic Themes" /></a>
                        </li>
                        <li>
                            <a href="/"> <img src={require("../images/press/femina.png")} alt="Energetic Themes" /></a>
                        </li>
                        <li>
                            <a href="/"> <img src={require("../images/press/whatsup.png")} alt="Energetic Themes" /></a>
                        </li>
                        {/*<li>
                            <a href="/"> <img src={require("../images/press/7.png")} alt="Energetic Themes" /></a>
                        </li>
                        <li>
                            <a href="/"> <img src={require("../images/press/9.png")} alt="Energetic Themes" /></a>
                        </li>*/}
                    </ul>
                </div>
            </div>
        </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    items: state
  }
}

export default connect(mapStateToProps, {addToCart})(App); 

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import * as firebase from 'firebase';
import {removeFromCart} from '../actions';
import _ from 'lodash';
import { bake_cookie, read_cookie, delete_cookie} from 'sfcookies';
import { AxiosProvider, Request, Get, Delete, Head, Post, Put, Patch, withAxios } from 'react-axios';
import axios from 'axios';
import '../cart.css';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    // right                 : 'auto',
    bottom                : 'auto',
    // // marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
  }
};



class Cart extends Component {

  constructor(props){
    super(props)
    this.state = {
      subtotal: 0,
      modalIsOpen: false,
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      address: '',
      invalid: true
    }
  }

  removeItem(id) {
    console.log('deleting in cart', id);
    console.log('this.props', this.props)
    this.props.removeFromCart(id)
  }

  openAddressModal(){
    console.log('i am here')
    this.setState({modalIsOpen: true});
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  afterOpenModal() {

  }

  payOut(amount) {

    window.Email.send("info@dobandar.com", "ajayaram@qwinix.io", "Test", "Testing 123", "smtp.elasticemail.com", "dobandar.17@gmail.com", "2b6d185d-16ad-4178-8c4c-4a6dd9ff6dab");

    const address = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      phone: this.state.phone,
      email: this.state.email,
      address: this.state.address
    }
    console.log('inside payout', address)
    bake_cookie('address', address)
    let options = {
      "key": "rzp_test_A1i48C8vwP08Gh",
      "amount": amount*100, // 2000 paise = INR 20, amount in paisa
      "name": "Merchant Name",
      "description": "Purchase Description",
      "image": "/your_logo.png",
      "handler": function (response){
        alert(response.razorpay_payment_id);
      },
      "prefill": {
        "name": address.first_name,
        "email": address.email
      },
      "notes": {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        phone: this.state.phone,
        email: this.state.email,
        address: this.state.address
      },
      "theme": {
        "color": "#F37254"
      }
    };
    const rzp = new window.Razorpay(options);
    rzp.open();
  }

  render() {
    const items = read_cookie('items');
    const total = _.sumBy(items, 'price')
    console.log('total', total)
    return(
      <div>
      <div className="home_1_testimonials_cont">
          <div className="container">
              <div className="row">
                  <div className="testimonials_title">
                      <h2>Your Cart</h2>
                  </div>
              </div>
          </div>
          </div>
      <div className="wrap">

  <header className="cart-header cf">
    <strong>Items</strong>
    <span className="btn">Checkout</span>
  </header>
  <div className="cart-table">
    <ul>
    {
              items.map(item => {
                return(

      <li key={item.id} className="item">
        <div className="item-main cf">
          <div className="item-block ib-info cf">
            <img className="product-img" src="https://firebasestorage.googleapis.com/v0/b/dobandar-2392f.appspot.com/o/Do%20Bandar%20-%202%20soaps%201%20foot%20softner%201%20scrub%201%20bath%20oil%20combo%20-%2001.jpg?alt=media&token=dbde8819-7bbd-43a7-a339-b4e9874606b3" />
            <div className="ib-info-meta">
              <span className="title">{item.product}</span>
              <span className="itemno"></span>
            </div>
          </div>
          <div className="item-block ib-qty">
            <input type="text" value={item.quantity} className="qty" />
            <span className="price"><span>x</span>Rs. { item.price}/-</span>
          </div>
          <div className="item-block ib-total-price">
            <span className="tp-price"></span>
            <span className="tp-remove"><i className="i-cancel-circle"></i></span>
          </div>         
        </div>
        <div className="item-foot cf">
          
        </div>
        </li>
        )
      })
    }
    </ul>

        </div>
        {/* <div className="item-foot cf">

          <div className="if-left"><span className="if-status">In Stock</span></div>
          <div className="if-right"><span className="blue-link">Gift Options</span> | <span className="blue-link">Add to Registry</span> | <span className="blue-link">Add to Wishlist</span></div>
        </div> */}
      
  {/* </div> */}
  <div className="sub-table cf">
    <div className="summary-block">
      <ul>
        <li className="subtotal"><span className="sb-label">Subtotal</span><span className="sb-value">Rs. {total}</span></li>
        <li className="shipping"><span className="sb-label">Shipping</span><span className="sb-value">Rs. {total > 100 ? 50:0}</span></li>
        {/* <li className="tax"><span className="sb-label">Est. Tax | <span className="tax-edit">edit <i className="i-notch-down"></i></span></span><span className="sb-value">$5.00</span></li> */}
        {/* <li className="tax-calculate"><input type="text" value="06484" className="tax" /><span className="btn">Calculate</span></li> */}
        <li className="grand-total"><span className="sb-label">Total</span><span className="sb-value">Rs. {total > 250 ? (total+50):total}</span></li>
      </ul>
    </div>
    <div className="copy-block">    
      {/* <p>Items will be saved in your cart for 30 days. To save items longer, add them to your <a href="#">Wish List</a>.</p> */}
      
    </div>    
  </div>
  
  <div className="cart-footer cf">
      <span className="btn" onClick={() => this.openAddressModal()}>Checkout</span>
      <span className="cont-shopping"><i className="i-angle-left"></i>Continue Shopping</span> 
  </div>
  <Modal
                isOpen={this.state.modalIsOpen}
                onAfterOpen={() => this.afterOpenModal() }
                onRequestClose={() => this.closeModal() }
                style={customStyles}
                contentLabel="Example Modal"
              >
       
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" onClick={() => this.closeModal()}>&times;</button>
                  <h4 className="modal-title">Shipping Address</h4>
                </div>
                <form>
                  <div className="modal-body">
                      <div className="form-group name">
                        <label for="first_name">First Name:</label>
                        <input type="text" className="form-control" id="first_name" onChange={(event) => this.setState({first_name: event.target.value})} required/>
                      </div>
                      <div className="form-group name">
                        <label for="last_name">Last Name:</label>
                        <input type="text" className="form-control" id="last_name" onChange={(event) => this.setState({last_name: event.target.value})} required/>
                      </div>
                      <div className="form-group">
                        <label for="phone">Phone:</label>
                        <input type="text" className="form-control" id="phone" onChange={(event) => this.setState({phone: event.target.value})} required/>
                      </div>
                      <div className="form-group">
                        <label for="email">Email:</label>
                        <input type="email" className="form-control" id="email" onChange={(event) => this.setState({email: event.target.value})} required/>
                      </div>
                      <div className="form-group">
                        <label for="addressess">Address:</label>
                        <textarea type="textarea" className="form-control" id="address" onChange={(event) => this.setState({address: event.target.value})} required/>
                      </div>
                  </div>
                  <div className="modal-footer">
                    <a href="#" className="btn continue" onClick={ () => this.payOut(total > 250 ? (total+50):total) }>Save & Pay Rs. {total > 250 ? (total+50):total}</a>
                  </div>
                </form>
              </Modal>
</div>
</div>
    )
  }
}


function mapStateToProps(state) {
  return {
    items: state
  }
}

export default connect(mapStateToProps, {removeFromCart})(Cart); 
import { ADD_TO_CART, REMOVE_FROM_CART } from '../constants'

export const addToCart = (product, quantity, price) => {
	const action = {
		type: ADD_TO_CART,
		quantity,
		product,
		price
	}

	console.log('action in addToCart', action)
	return action
}

export const removeFromCart = (id) => {
	const action = {
		type: REMOVE_FROM_CART,
		id
	}
	console.log('action in removeFromCart', action)
	return action;
}